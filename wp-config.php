<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'basededatos' );

/** MySQL database username */
define( 'DB_USER', 'usuario2' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'db-aurora57-instance-1.c2d3rjlsslb7.eu-west-1.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2DFr]A:2xvIVrm:eA#@))^=g[;UVHJn)z,/|>*;qgph_.F/3;vgaT^KHAZ}~g1e`' );
define( 'SECURE_AUTH_KEY',  'tu_Gu y@.Lt`gpRRyWa0l-]rmclqrrx#mM/ts5$sjtqLwhO=Oc$:7HqvE5UJ)h>x' );
define( 'LOGGED_IN_KEY',    '3I.P,{HWQib)R~AZUpK^ *x<%W(F0pUj0Yf%.#i!,h?SaENgM$CL~cNc5R+ ew}y' );
define( 'NONCE_KEY',        '36*Zq<3DGl:h_7eWPAld%@1Mb]LL,$x6[tA#+`fO@n@qVf`I2:S)++ O./DGxF_@' );
define( 'AUTH_SALT',        'N|4Y;HFW^v]kx5,I=>r{Sa#mKoqu]*n%r 2jx2H[7Ompr<dkzHa@Wp6 UZ3Ha6$T' );
define( 'SECURE_AUTH_SALT', 'A4K|nFo|,3i{1V3h^N-+A?o=Bni4}~mqc`*<,BkxWuE&7dg`ku3}uv8-S-,%Oh=E' );
define( 'LOGGED_IN_SALT',   'y(5Rw(wJfR/(+7G^RQ8@~tCi>huq57,(rQ$2B9$<TJ2qGaEwa7FA3n:5`~m]ufwQ' );
define( 'NONCE_SALT',       'Xt[gF]Oy9/M&H-/l=iDFQ_hZ0HlZQ9Eo<ty(zP O) l(Z/+M{yOfe4]W4T#@SV4B' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
